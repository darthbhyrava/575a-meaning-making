# 575a-meaning-making

User-Study data for my Term Paper for 575A: Meaning Making (Dr. Emily Bender) at the University of Washington.

- `questionnaire.pdf` contains the 1.5 hour user-facing study instructions. 
- `user_studies/chats` contains conversation history of sample user studies
- `user_studies/responses` contains questionnaires filled in during sample user studies